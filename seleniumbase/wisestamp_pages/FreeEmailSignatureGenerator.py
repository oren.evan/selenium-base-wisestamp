from selenium.webdriver.common.by import By

from seleniumbase import BaseCase


class FreeEmailSignatureGenerator(BaseCase):
    def __init__(self, basecase):
        self.basecase = basecase
        self.YOURNAME_INPUT = '//*[@id=\"details\"]/div/div[1]/ws-input[1]/div/input'

    def update_your_name(self,newname):
        self.basecase.assert_element(self.YOURNAME_INPUT)
        self.basecase.click(self.YOURNAME_INPUT)
        self.basecase.update_text(self.YOURNAME_INPUT,newname)

