from selenium.webdriver.common.by import By
from seleniumbase.wisestamp_pages.LoginPage import LoginPage
from selenium.webdriver.support.wait import WebDriverWait


class MainPage():
    def __init__(self, basecase):
        self.basecase = basecase
        self.LOGIN_BTN = (By.CSS_SELECTOR, 'ws - navigation - top - -login_link')
        self.LOGIN_BTN_GOOGLE = 'div.connect-btns > div.btn.google.transition.scale'
        # LOGIN_BTN = (By.ID, 'ws - navigation - login')
        self.LOGO = (By.CSS_SELECTOR, '.ws-navigation-top--logo nav_icon_wisestamp')

    def click_login_button(self):
        basecase = self.basecase
        basecase.assert_element(self.LOGIN_BTN_GOOGLE)  # Assert element on page
        basecase.click(self.LOGIN_BTN_GOOGLE)
        # wait to make sure there are two windows open
        WebDriverWait(self, 10).until(lambda d: len(self.basecase.driver.window_handles) == 2)
        # switch windows
        basecase.driver.switch_to_window(self.basecase.driver.window_handles[1])
        # wait to make sure the new window is loaded
        WebDriverWait(self, 10).until(lambda d: self.basecase.driver.title != "")
        return LoginPage(self.basecase)

