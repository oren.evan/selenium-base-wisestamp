from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from seleniumbase.wisestamp_pages.SignInGooglePassword import SignInGooglePassword
from seleniumbase import BaseCase

class SignInGoogleEmail(BaseCase):
    def __init__(self, driver):
        super(SignInGoogleEmail, self).__init__(driver)  # Python2 version

    NEXT_BTN = (By.CSS_SELECTOR, '#identifierNext')
    LOGO = (By.CSS_SELECTOR, '.service - logo')
    EMAIL = (By.CSS_SELECTOR, 'input[type=\"email\"')

    def enter_email(self):
        self.find_element(*self.EMAIL).send_keys("oren.evan@gmail.com")

    def check_page_loaded(self):
        return True if self.find_element(*self.LOGO) else False

    def click_next_button(self):
        self.find_element(*self.NEXT_BTN).click()
        # wait to make sure there are two windows open
        WebDriverWait(self.driver, 10).until(lambda d: len(d.window_handles) == 2)
        # switch windows
        self.driver.switch_to_window(self.driver.window_handles[1])
        # wait to make sure the new window is loaded
        WebDriverWait(self.driver, 10).until(lambda d: d.title != "")
        return SignInGooglePassword(self.driver)
