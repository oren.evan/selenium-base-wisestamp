from selenium.webdriver.support.wait import WebDriverWait
from seleniumbase import BaseCase
from seleniumbase.wisestamp_pages.FreeEmailSignatureGenerator import FreeEmailSignatureGenerator
import time
import logging


class SignInGooglePassword(BaseCase):
    def __init__(self, basecase):
        self.basecase = basecase
        self.PASSWORD_NEXT_BTN = '#passwordNext'
        self.LOGO = '.service - logo'
        self.INPUT_PASSWORD = 'input[type=\"password\"]'
        self.HARD_TIME_OUT_FOR_SIGN_IN_COMPLETE = 5
        self.ITS_TIME_YOU_STEP_UP_YOURNAME_CLOSE = "#hellobar i"

    def pass_google_password(self, password):
        self.basecase.assert_element(self.INPUT_PASSWORD)
        self.basecase.update_text(self.INPUT_PASSWORD, password)
        self.basecase.assert_element(self.PASSWORD_NEXT_BTN)
        self.basecase.click(self.PASSWORD_NEXT_BTN)
        # wait to make sure there are two windows open
        WebDriverWait(self.basecase.driver, 10).until(lambda d: len(d.window_handles) == 2)
        # switch windows
        self.basecase.driver.switch_to_window(self.basecase.driver.window_handles[1])
        # wait to make sure the new window is loaded
        WebDriverWait(self.basecase.driver, 10).until(lambda d: d.title != "")

        for handle in self.basecase.driver.window_handles:
            self.basecase.driver.switch_to.window(handle)
        time.sleep(self.HARD_TIME_OUT_FOR_SIGN_IN_COMPLETE)

        for handle in self.basecase.driver.window_handles:
            self.basecase.driver.switch_to.window(handle)

            if not self.basecase.is_element_visible(self.ITS_TIME_YOU_STEP_UP_YOURNAME_CLOSE):
                logging.debug("Red Alert: window its_time_you_step_up_your_game wasnt shown")
            else:
                self.basecase.click(self.ITS_TIME_YOU_STEP_UP_YOURNAME_CLOSE)
            # element = WebDriverWait(driver, 10).until(
            #    EC.presence_of_element_located((By.CSS_SELECTOR, ITS_TIME_YOU_STEP_UP_YOURNAME_CLOSE))
            # )
            # its_time_you_step_up_your_game = driver.find_element_by_css_selector(ITS_TIME_YOU_STEP_UP_YOURNAME_CLOSE)
            # its_time_you_step_up_your_game.click()
            # except TimeoutError:
            # print("Timoeout window its_time_you_step_up_your_game wasn't shown")
            # finally:
            # print("window its_time_you_step_up_your_game wasn't shown")
        return FreeEmailSignatureGenerator(self.basecase)

    def click_next_button(self):
        self.find_element(*self.PASSWORD_NEXT_BTN).click()
        # wait to make sure there are two windows open
        WebDriverWait(self.driver, 10).until(lambda d: len(d.window_handles) == 1)
        # switch windows
        self.driver.switch_to_window(self.driver.window_handles[0])
        # wait to make sure the new window is loaded
        WebDriverWait(self.driver, 10).until(lambda d: d.title != "")
        return FreeEmailSignatureGenerator(self.driver)

    def is_selected_password(self):
        self.find_element(*self.PASSWORD).is_selected()
