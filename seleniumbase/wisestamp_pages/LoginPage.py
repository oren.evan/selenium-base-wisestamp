from selenium.webdriver.common.by import By

from seleniumbase.wisestamp_pages.SignInGoogleEmail import SignInGoogleEmail
from selenium.webdriver.support.wait import WebDriverWait
from seleniumbase.wisestamp_pages.SignInGooglePassword import SignInGooglePassword


class LoginPage():
    def __init__(self, basecase):
        self.basecase = basecase
        self.EMAIL_NEXT_BTN = '#identifierNext'

    INPUT_EMAIL = 'input[type=\"email\"'

    LOGIN_GOOGLE_BTN = (By.CSS_SELECTOR, 'div.connect-btns > div.btn.google.transition.scale')
    LOGO = (By.CSS_SELECTOR, '.service - logo')
    EMAIL_TEXT = (By.CSS_SELECTOR, 'input#login_email')
    PASSWORD_TEXT = (By.CSS_SELECTOR, 'input#login_password')
    LOGIN_BTN = (By.CSS_SELECTOR, 'button#login_button')
    ALREADY_GOT_ACCOUNT_BTN = (By.CSS_SELECTOR, '.login-now-btn')
    FORGOT_PASSWORD_LINK = (By.CSS_SELECTOR, 'a[href=\"/forgot_password\"]')
    CREATE_ACCOUNT = (By.CSS_SELECTOR, 'div.form.login > div.form-links > span[ng-click=\"fn.switchForms()\"]')

    def click_login_button(self):
        self.find_element(*self.locator.SUBMIT).click()

    def pass_google_email(self, user):
        self.basecase.assert_element(self.INPUT_EMAIL)
        self.basecase.update_text(self.INPUT_EMAIL, user)
        self.basecase.assert_element(self.EMAIL_NEXT_BTN)
        self.basecase.click(self.EMAIL_NEXT_BTN)
        # wait to make sure there are two windows open
        WebDriverWait(self.basecase.driver, 10).until(lambda d: len(d.window_handles) == 2)
        # switch windows
        self.basecase.driver.switch_to_window(self.basecase.driver.window_handles[1])
        # wait to make sure the new window is loaded
        WebDriverWait(self.basecase.driver, 10).until(lambda d: d.title != "")
        return SignInGooglePassword(self.basecase)

    def login_with_in_valid_user(self, user):
        self.login(user)
        return self.find_element(*self.locator.ERROR_MESSAGE).text

    def check_page_loaded(self):
        return True if self.find_element(*self.locator.LOGO) else False

    def click_login_google_button(self):
        self.find_element(*self.LOGIN_GOOGLE_BTN).click()
        # wait to make sure there are two windows open
        WebDriverWait(self.driver, 10).until(lambda d: len(d.window_handles) == 2)
        # switch windows
        self.driver.switch_to_window(self.driver.window_handles[1])
        # wait to make sure the new window is loaded
        WebDriverWait(self.driver, 10).until(lambda d: d.title != "")
        return SignInGoogleEmail(self.driver)
